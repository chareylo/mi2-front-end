<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="description" content="Agence de voyage à Grenoble">
    <script type="text/javascript" src="JS/menu.js"></script>
    <link href="style.css" rel="stylesheet">
    <title>Connexion</title>
</head>
<body>
    <div class="page-container">
        <h1>Agence de voyage à Grenoble</h1>
        <hr>
        <header>
            <nav id="menu">
            </nav>
        </header>
            <?php
            // On detruit la session pour se reconnecter
            session_start();
            $_SESSION = array();
            session_destroy();

            $login_normal = "user";
            $login_admin = "admin";
            $password = "ajax";

            if(isset($_POST['login']) && isset($_POST['password'])) {
                if($_POST['login'] == $login_normal && $_POST['password'] == $password){
                    session_start();
                    $_SESSION['user'] = $login_normal;
                    echo "<p class='succes-login'>Vous êtes connecté en temps que : user</p>";
                }
                else if ($_POST['login'] == $login_admin && $_POST['password'] == $password) {
                    session_start();
                    $_SESSION['user'] = $login_admin;
                    echo "<p class='succes-login'>Vous êtes connecté en temps que : admin</p>";
                }
                else {
                    echo "<p class='erreur-login'>Mauvais mot de passe / login</p>";
                }
            }
            else {
                echo "<p class='erreur-login'>Vous n'êtes pas connecté</p>";
            }
            ?>

        <form class="login-form" action="login.php" method="POST">
            <label><b>Nom d'utilisateur</b></label>
            <input type="text" placeholder="Login" name="login" required>

            <label><b>Mot de passe</b></label>
            <input type="password" placeholder="Mot de passe" name="password" required>

            <input class="formSubmit" type="submit" id='submit' value='Se connecter' >
        </form>

    </div>
</body>
</html>
