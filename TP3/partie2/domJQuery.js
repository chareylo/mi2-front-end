let france = ['Paris', 'Marseille', 'Lyon', 'Grenoble'];
let allemagne = ['Berlin', 'Munich', 'Hambourg', 'Cologne'];
let espagne = ['Madrid', 'Barcelone', 'Seville', 'Bibao'];
let angleterre = ['Londres', 'Liverpool', 'Manchester', 'Bristol'];

function addDestination(country) {
    let listVilles = $('listVilles');
    let ville = $("ville").value.toString();
    let choix = window.confirm("Etes-vous d\'accord ?")
    if (choix !== false) {
        if (country === 'France') {
            france.push(ville)
            listVilles.prepend("<p>" + ville + "</p>");
            console.log(france);
        } else if (country === 'Allemagne') {
            allemagne.push(ville)
            listVilles.prepend("<p>" + ville + "</p>");
            console.log(allemagne);
        } else if (country === 'Espagne') {
            espagne.push(ville)
            listVilles.prepend("<p>" + ville + "</p>");
            console.log(espagne);
        } else if (country === 'Angleterre') {
            angleterre.push(ville)
            listVilles.prepend("<p>" + ville + "</p>");
            console.log(angleterre);
        }
    }
}

function chooseDestination() {
    let ville = $("ville").value.toString();
    let country;
    let array;
    if (france.includes(ville)) {
        country = 'France';
        array = france;
    } else if (allemagne.includes(ville)) {
        country = 'Allemagne';
        array = allemagne;
    } else if (espagne.includes(ville)) {
        country = 'Espagne';
        array = espagne;
    } else if (angleterre.includes(ville)) {
        country = 'Anglettere';
        array = angleterre;
    } else {
        window.alert('Ville non reconnue');
        return false;
    }

    let button = $('chooseDest');
    button.prepend("<h3>Bienvenue en " + country + " </h3><p id='listVilles'>Vous pouvez également visiter les lieux suivants :</p>");

    const iterator = array.values();
    for (const value of iterator) {
        let listVilles = $('listVilles');
        listVilles.prepend("<p>" + value + "</p>");
    }

    button.innerHTML = "Ajouter une destination";
    button.attr('onclick', "addDestination('" + country + "')");
}