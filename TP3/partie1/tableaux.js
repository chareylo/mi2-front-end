let france = ['Paris', 'Marseille', 'Lyon', 'Grenoble'];
let allemagne = ['Berlin', 'Munich', 'Hambourg', 'Cologne'];
let espagne = ['Madrid', 'Barcelone', 'Seville', 'Bibao'];
let angleterre = ['Londres', 'Liverpool', 'Manchester', 'Bristol'];

function getCountry() {
    let ville = document.getElementById("ville").value.toString();

    if (france.includes(ville)) {
        window.alert('Bienvenue en France');
    } else if (allemagne.includes(ville)) {
        window.alert('Bienvenue en Allemagne');
    } else if (espagne.includes(ville)) {
        window.alert('Bienvenue en Espagne');
    } else if (angleterre.includes(ville)) {
        window.alert('Bienvenue en Angleterre');
    } else {
        window.alert('Ville non reconnue');
        return false;
    }
}