<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="description" content="Agence de voyage à Grenoble">
    <link href="style.css" rel="stylesheet">
    <script type="text/javascript" src="JS/destinations.js"></script>
    <script type="text/javascript" src="JS/menu.js"></script>
    <title>Agence de voyage à Grenoble, 2 Rue de La Fontaine, Réservez sur le site ou en agence de voyages vos vacances de rêve grâce aux bons plans hôtels, séjours, circuits, locations ...</title>
    <style>
        <?php
            session_start();
            $login_normal = "user";
            $login_admin = "admin";
            $password = "ajax";

            if(isset($_SESSION['user'])) {
                // Connecté normal
                if($_SESSION['user'] == $login_normal){
                    // On n'affiche que le bouton decouvrir (on masque ajouter, supprimer, modifier)
                    echo ".supprimer, .modifier, #formDest {display: none;}.decouvrir {display: block;}";
                }
                // Connecté admin
                else if ($_SESSION['user'] == $login_admin) {
                    // On affiche tout sauf le bouton decouvrir
                    echo ".decouvrir {display: none;}";
                }
            }
            // Non connecté
            else {
                // On masque tout
                echo ".supprimer, .modifier, .decouvrir, #formDest {display: none;}";
            }
            ?>
    </style>
</head>

        <?php
        // Récuperation du cookie envoyé en JS pour ajout json
        if ($_COOKIE['newCountry'] || $_COOKIE['newCountry'] != '') {
            $newCountry = $_COOKIE['newCountry'];
            // On recupere le contenu du json puis le transforme en objet PHP
            $source = file_get_contents(__DIR__.'/JS/source.json');
            $sourceArray = json_decode($source);
            // Idem avec le contenu du cookie
            $newCountry = json_decode($newCountry);
            // Ensuite on merge les 2 objets ensemble
            $mergedArray = (object) array_merge((array) $sourceArray, (array) $newCountry);
            // Et enfin on écrit dans le json l'object mergé
            $jsonData = json_encode($mergedArray);
            file_put_contents(__DIR__.'/JS/source.json', $jsonData);
            // On supprime le cookie pour pouvoir recréer la valeur plus tard
            $_COOKIE['newCountry'] = '';
            setcookie($newCountry, "", time() - 3600);
        }

        // Récuperation du cookie envoyé en JS pour suppression json
        if ($_COOKIE['removeCountry'] || $_COOKIE['removeCountry'] != '') {
            $removeCountry = $_COOKIE['removeCountry'];
            // L'object json est toujours stocké en minuscule
            $removeCountry = strtolower($removeCountry);
            // On recupere le contenu du json puis le transforme en objet PHP
            $source = file_get_contents(__DIR__.'/JS/source.json');
            $sourceArray = json_decode($source);
            // Puis on supprime l'object du tableau
            unset($sourceArray->$removeCountry);
            // Et enfin on écrit dans le json
            $jsonData = json_encode($sourceArray);
            file_put_contents(__DIR__.'/JS/source.json', $jsonData);
            // On supprime le cookie pour pouvoir recréer la valeur plus tard
            $_COOKIE['removeCountry'] = '';
            setcookie($removeCountry, "", time() - 3600);
        }
        ?>

<body>
<div class="page-container">
    <h1>Agence de voyage à Grenoble</h1>
    <hr>
    <header>
        <nav id="menu">
        </nav>
    </header>

    <table id="table">
        <thead>
        <tr>
            <th colspan="5" class="table-title">Destinations</th>
        </tr>
        </thead>
        <tr>
            <th>Pays</th>
            <th>Image</th>
            <th>Tarif</th>
            <th>Réservation</th>
            <th>Editer</th>
        </tr>
        <tbody id="tableau-destinations">
        </tbody>
    </table>

    <br>

    <form id="formDest" action="" onsubmit="addDestination(event)">
        <fieldset>
            <legend id="fieldset">Ajouter une destination</legend>
            <label for="pays">Pays : </label>
            <input required type="text" id="pays">
            <label for="img_src">Source image : </label>
            <input required type="text" id="img_src" value="image/paris.webp">
            <label for="tarif">Tarif (€) : </label>
            <input required type="number" id="tarif" value="250">
            <label for="nb_places">Nb places : </label>
            <input required type="number" id="nb_places" value="10">
            <input class="formSubmit" type="submit" id="chooseDest" value="Ajouter une destination">
        </fieldset>
    </form>

    <br>

    <a href="#table">Revenir en haut</a>
</div>

<footer>Made with ❤️ by Loic Chareyron</footer>

</body>
</html>
