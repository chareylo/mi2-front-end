let destinations = {
    france: { pays: 'France', img_src: 'image/paris.webp', tarif: 500, nb_places: 8 },
    allemagne: { pays: 'Allemagne', img_src: 'image/allemagne.jpg', tarif: 600, nb_places: 7 },
    espagne: { pays: 'Espagne', img_src: 'image/espagne.webp', tarif: 350, nb_places: 12 },
    belgique: { pays: 'Belgique', img_src: 'image/belgique.webp', tarif: 380, nb_places: 4 }
};

window.addEventListener("load", demarrer);

function demarrer() {
    // Affichage du tableau destinations depuis le JS
    let tableau = document.getElementById('tableau-destinations');
    for (const [key, value] of Object.entries(destinations)) {
        tableau.insertAdjacentHTML('beforeend', "<tr><td class='td-pays'>" + value.pays + "</td><td><img class='td-image' src=\"" + value.img_src + "\" alt=\"" + key + "\" width=\"200\"></td><td class='td-tarif'>" + value.tarif + "€</td><td class='td-nb_places'>" + value.nb_places + " place(s)</td><td><button class='supprimer' onclick='removeDestination(event)'>Supprimer</button><button class='modifier' onclick='updateDestinationForm(event)'>Modifier</button></td></tr>");
        console.log(`${key}: ${value.pays}`);
    }
}

function addDestination(e) {
    e.preventDefault();
    let tableau = document.getElementById('tableau-destinations');

    // Récuperation des valeurs du formulaire
    let pays = document.getElementById("pays").value;
    let paysKey = document.getElementById("pays").value.toString().toLowerCase();
    let img_src = document.getElementById("img_src").value;
    let tarif = document.getElementById("tarif").value;
    let nb_places = document.getElementById("nb_places").value;

    // Ajout des valeurs au tableau
    const newCountry = { [paysKey]: { pays: pays, img_src: img_src, tarif: tarif, nb_places: nb_places } };
    Object.entries(newCountry).forEach(([key,value]) => { destinations[key] = value })
    tableau.insertAdjacentHTML('beforeend', "<tr><td class='td-pays'>" + pays + "</td><td><img class='td-image' src=\"" + img_src + "\" alt=\"" + paysKey + "\" width=\"200\"></td><td class='td-tarif'>" + tarif + "€</td><td class='td-nb_places'>" + nb_places + " place(s)</td><td><button class='supprimer' onclick='removeDestination(event)'>Supprimer</button><button class='modifier' onclick='updateDestinationForm(event)'>Modifier</button></td></tr>");
}

function removeDestination(e) {
    e.currentTarget.parentNode.parentNode.remove();
}

function updateDestinationForm(e) {
    e.preventDefault();
    // Modification du formulaire
    let form = document.getElementById('formDest');
    let fieldset = document.getElementById('fieldset');
    let submit = document.getElementById('chooseDest');
    form.style.border = "3px solid #2c69f6";
    fieldset.innerHTML = "Modifier une destination";
    submit.setAttribute("value", "Modifier une destination");
    submit.style.backgroundColor = "#2c69f6";
    form.setAttribute("onsubmit", "updateDestination(event)");

    // Ajout id à la ligne du tableau a modifier pour retrouver plus tard
    e.currentTarget.parentElement.parentElement.setAttribute('id', "aModif");
}

function updateDestination(e) {
    e.preventDefault();

    // Récuperation des valeurs du formulaire
    let pays = document.getElementById("pays").value;
    let paysKey = document.getElementById("pays").value.toString().toLowerCase();
    let img_src = document.getElementById("img_src").value;
    let tarif = document.getElementById("tarif").value;
    let nb_places = document.getElementById("nb_places").value;

    // Modification des valeurs de la ligne du tableau
    let ligneTableau = document.getElementById("aModif");
    ligneTableau.getElementsByClassName("td-pays")[0].innerHTML = pays;
    ligneTableau.getElementsByClassName("td-image")[0].setAttribute("src", img_src);
    ligneTableau.getElementsByClassName("td-tarif")[0].innerHTML = tarif + "€";
    ligneTableau.getElementsByClassName("td-nb_places")[0].innerHTML = nb_places + " place(s)";

    // Reset du formulaire pour revenir sur "Ajouter une destination"
    let form = document.getElementById('formDest');
    let fieldset = document.getElementById('fieldset');
    let submit = document.getElementById('chooseDest');
    form.style.border = "none";
    fieldset.innerHTML = "Ajouter une destination";
    submit.setAttribute("value", "Ajouter une destination");
    submit.style.backgroundColor = "forestgreen";
    form.setAttribute("onsubmit", "addDestination(event)");

    // Suppression de l'id "aModif"
    document.getElementById("aModif").removeAttribute('id');
}


