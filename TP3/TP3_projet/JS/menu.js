let menus = {
    accueil: { name: 'Accueil', href: 'home.html' },
    perso: { name: 'Espace perso', href: 'perso.html', content: { content1: 'Connexion', content2: 'Mes infos', content3: 'Messagerie', content4: 'Historique' } },
    destination: { name: 'Destinations', href: 'destinations.php' },
    audio: { name: 'Audio', href: 'audio.html' },
    video: { name: 'Video', href: 'video.html' },
    contact: { name: 'Contact', href: 'contact.html' }
};

window.addEventListener("load", demarrer);

function demarrer() {
    // Affichage du menu depuis le JS
    let menu = document.getElementById('menu');
    for (const [key, value] of Object.entries(menus)) {
        // Si value.content existe, on à une liste déroulante à afficher
        if (value.content) {
            menu.insertAdjacentHTML('beforeend', "<div class=\"dropdown\"><button class=\"dropbtn\">" + value.name + "</button><div class=\"dropdown-content\"><a href=\"" + value.href + "\">" + value.content.content1 + "</a><a href=\"" + value.href + "\">" + value.content.content2 + "</a><a href=\"" + value.href + "\">" + value.content.content3 + "</a><a href=\"" + value.href + "\">" + value.content.content4 + "</a></div></div>");
        } else {
            menu.insertAdjacentHTML('beforeend', "<a href=\"" + value.href + "\">" + value.name + "</a>");
        }
        console.log(`${key}: ${value.name}`);
    }
}