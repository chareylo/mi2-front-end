const randomNumber = Math.floor(Math.random() * 11).toString();
let reponse = window.prompt('Choisir un nombre entre 0 et 10', 'Saisir votre réponse');
let nbEssai = 1;

while (reponse !== randomNumber && nbEssai < 3) {
    if (reponse > randomNumber) {
        reponse = window.prompt('C\'est moins !', 'Saisir votre réponse');
    }
    if (reponse < randomNumber) {
        reponse = window.prompt('C\'est plus !', 'Saisir votre réponse');
    }
    nbEssai++;
}
if (nbEssai >= 3) {
    window.alert('Auncuns essais restants !');
}